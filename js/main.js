$(document).ready(function() {
    function refresh() {
        $.ajax({
            url: '../php/refresh.php',
            type: 'post',
            success: function(json) {
                for (i = 0;i < json.length;i++) {
                    $('#chatDisplay').append("<strong>"+json[i].Name+":</strong> "
                                              +json[i].Text+"<br>");
                }
            }
        });
    }

    $("#chatSubmitForm").submit(function() {
        
        $.ajax({
            url: '../php/insert.php',
            type: 'post',
            data: {
                name: $("#chatName").val(),
                text: $("#chatText").val()
            }
        });
        
        $("#chatText").val("");
        return false;
    });
    
    setInterval(refresh, 500);
    refresh();
    
});
