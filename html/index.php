<?php require('../php/init.php'); 
?>
<!DOCTYPE html>
<html>
<head>
  <title>jChat</title>
  <script src='http://code.jquery.com/jquery-2.1.4.min.js'></script>
  <script src='../js/main.js'></script>
  <link type='text/css' rel='stylesheet' href='../css/main.css'>
</head>
<body>
  <div id='chatContainer'>
    <h1>jChat</h1>
    <div id='chatDisplay'>
    </div>
    <form id='chatSubmitForm'>
      <div id='chatBarContainer'>
        <div id='chatBar'>
          <label>Name: </label><input id='chatName' type='text'>
          <label>Chat: </label><input id='chatText' type='text'>
          <button id='chatSubmit' type='submit'>Send</button>
        </div>
      </div>
    </form>
  </div>
</body>
</html>
