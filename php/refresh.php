<?php
    require("config.php");
    header("content-type:application/json");
    session_start();
    
    //connect to DB
    $dbh = new PDO($connectString, $dbUser, $dbPass);
    
    $select = "SELECT Name, Text
               FROM ChatLines
               WHERE LineID > {$_SESSION['lastLine']}";
    $chatLines = prepStatement($dbh, $select);
    
    $sqlLastLine = prepStatement($dbh,
        "SELECT   LineID
        FROM     ChatLines
        ORDER BY LineID DESC
        LIMIT    1;");
    $_SESSION['lastLine'] = $sqlLastLine[0]['LineID'];
    
    echo json_encode($chatLines);
?>
