<?php
    session_start();
    require_once("config.php");
    
    //connect to DB
    $dbh = new PDO($connectString, $dbUser, $dbPass);
    
    $sqlLastLine = prepStatement($dbh,
        "SELECT   LineID
        FROM     ChatLines
        ORDER BY LineID DESC
        LIMIT    1;");
    
    $_SESSION['lastLine'] = $sqlLastLine[0]['LineID'];
?>
